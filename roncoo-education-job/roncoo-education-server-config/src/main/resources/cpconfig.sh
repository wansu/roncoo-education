#!/bin/zsh
spring_cloud_config_git_path=/Users/franck/develop/franck/code/roncoo/roncoo-education/springcloud-config
cd config/master
rm -rf $spring_cloud_config_git_path/*
cp ./* $spring_cloud_config_git_path
cd $spring_cloud_config_git_path
git add -A
git commit -m "update"
git push
